package com.cashcare.config;

/**
 * Application constants.
 */
public final class Constants {

    public static final String HOUSE_REGEX = "^[a-zA-z\\d.]*$";
    public static final String USER_REGEX = "^((?:[a-zA-Z]{2,})(?:[0-9]{0,3}))$";
    public static final String LETTER_REGEX = "^[A-Za-z ]+$";
    public static final String USER_BCRYPT_REGEX = "^\\$2[aby]?\\$\\d{1,2}\\$[.\\/A-Za-z0-9]{53}$";
    public static final String PHONE_REGEX = "^\\d*$";
    public static final String POSTAL_REGEX = "^\\b((?:0[1-46-9]\\d{3})|(?:[1-357-9]\\d{4})|(?:[4][0-24-9]\\d{3})|(?:[6][013-9]\\d{3}))\\b$";
    public static final String ACCOUNTNUMBER_REGEX = "^\\d{10}$";

    public static final String SYSTEM_ACCOUNT = "system";
    public static final String DEFAULT_LANGUAGE = "en";
    public static final String ANONYMOUS_USER = "anonymoususer";


    private Constants() {
    }
}
