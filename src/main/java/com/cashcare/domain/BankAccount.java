package com.cashcare.domain;


import com.cashcare.config.Constants;
import org.hibernate.cache.spi.support.CollectionNonStrictReadWriteAccess;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A BankAccount.
 */
@Entity
@Table(name = "bank_account")
public class BankAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Pattern(regexp = Constants.ACCOUNTNUMBER_REGEX)
    @Column(name = "iban", nullable = false)
    private String iban;

    @NotNull
    @Column(name = "balance", nullable = false)
    private Float balance = 1000f;

    @NotNull
    @Column(name = "archived", nullable = false)
    private Boolean archived = false;

    @OneToOne
    @JoinColumn(unique = true)
    private Customer customer;

    @OneToMany(mappedBy = "bankAccountFrom")
    private Set<StandingOrder> sendStandingOrders = new HashSet<>();

    @OneToMany(mappedBy = "bankAccountTo")
    private Set<StandingOrder> receiveStandingOrders = new HashSet<>();

    @OneToMany(mappedBy = "bankAccountFrom")
    private Set<Transaction> sentTransactions = new HashSet<>();

    @OneToMany(mappedBy = "bankAccountTo")
    private Set<Transaction> receivedTransactions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIban() {
        return iban;
    }

    public BankAccount iban(String iban) {
        this.iban = iban;
        return this;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public Float getBalance() {
        return balance;
    }

    public BankAccount balance(Float balance) {
        this.balance = balance;
        return this;
    }

    public void setBalance(Float balance) {
        this.balance = balance;
    }

    public Boolean isArchived() {
        return archived;
    }

    public BankAccount archived(Boolean archived) {
        this.archived = archived;
        return this;
    }

    public void setArchived(Boolean archived) {
        this.archived = archived;
    }

    public Customer getCustomer() {
        return customer;
    }

    public BankAccount customer(Customer customer) {
        this.customer = customer;
        return this;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Set<StandingOrder> getSendStandingOrders() {
        return sendStandingOrders;
    }

    public BankAccount sendStandingOrders(Set<StandingOrder> standingOrders) {
        this.sendStandingOrders = standingOrders;
        return this;
    }

    public BankAccount addSendStandingOrder(StandingOrder standingOrder) {
        this.sendStandingOrders.add(standingOrder);
        standingOrder.setBankAccountFrom(this);
        return this;
    }

    public BankAccount removeSendStandingOrder(StandingOrder standingOrder) {
        this.sendStandingOrders.remove(standingOrder);
        standingOrder.setBankAccountFrom(null);
        return this;
    }

    public void setSendStandingOrders(Set<StandingOrder> standingOrders) {
        this.sendStandingOrders = standingOrders;
    }

    public Set<StandingOrder> getReceiveStandingOrders() {
        return receiveStandingOrders;
    }

    public BankAccount receiveStandingOrders(Set<StandingOrder> standingOrders) {
        this.receiveStandingOrders = standingOrders;
        return this;
    }

    public BankAccount addReceiveStandingOrder(StandingOrder standingOrder) {
        this.receiveStandingOrders.add(standingOrder);
        standingOrder.setBankAccountTo(this);
        return this;
    }

    public BankAccount removeReceiveStandingOrder(StandingOrder standingOrder) {
        this.receiveStandingOrders.remove(standingOrder);
        standingOrder.setBankAccountTo(null);
        return this;
    }

    public void setReceiveStandingOrders(Set<StandingOrder> standingOrders) {
        this.receiveStandingOrders = standingOrders;
    }

    public Set<Transaction> getSentTransactions() {
        return sentTransactions;
    }

    public BankAccount sentTransactions(Set<Transaction> transactions) {
        this.sentTransactions = transactions;
        return this;
    }

    public BankAccount addSentTransaction(Transaction transaction) {
        this.sentTransactions.add(transaction);
        transaction.setBankAccountFrom(this);
        return this;
    }

    public BankAccount removeSentTransaction(Transaction transaction) {
        this.sentTransactions.remove(transaction);
        transaction.setBankAccountFrom(null);
        return this;
    }

    public void setSentTransactions(Set<Transaction> transactions) {
        this.sentTransactions = transactions;
    }

    public Set<Transaction> getReceivedTransactions() {
        return receivedTransactions;
    }

    public BankAccount receivedTransactions(Set<Transaction> transactions) {
        this.receivedTransactions = transactions;
        return this;
    }

    public BankAccount addReceivedTransaction(Transaction transaction) {
        this.receivedTransactions.add(transaction);
        transaction.setBankAccountTo(this);
        return this;
    }

    public BankAccount removeReceivedTransaction(Transaction transaction) {
        this.receivedTransactions.remove(transaction);
        transaction.setBankAccountTo(null);
        return this;
    }

    public void setReceivedTransactions(Set<Transaction> transactions) {
        this.receivedTransactions = transactions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BankAccount)) {
            return false;
        }
        return id != null && id.equals(((BankAccount) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BankAccount{" +
            "id=" + getId() +
            ", iban='" + getIban() + "'" +
            ", balance=" + getBalance() +
            ", archived='" + isArchived() + "'" +
            "}";
    }
}
