package com.cashcare.domain;

import com.cashcare.config.Constants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A StandingOrder.
 */
@Entity
@Table(name = "standing_order")
public class StandingOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "amount", nullable = false)
    private Float amount;

    @NotNull
    @Pattern(regexp = Constants.USER_REGEX)
    @Column(name = "jhi_user", nullable = false)
    private String user;

    @NotNull
    @Column(name = "excecution_day", nullable = false)
    private Integer excecutionDay;

    @ManyToOne
    @JsonIgnoreProperties(value = "sendStandingOrders", allowSetters = true)
    private BankAccount bankAccountFrom;

    @ManyToOne
    @JsonIgnoreProperties(value = "receiveStandingOrders", allowSetters = true)
    private BankAccount bankAccountTo;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getAmount() {
        return amount;
    }

    public StandingOrder amount(Float amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getUser() {
        return user;
    }

    public StandingOrder user(String user) {
        this.user = user;
        return this;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Integer getExcecutionDay() {
        return excecutionDay;
    }

    public StandingOrder excecutionDay(Integer excecutionDay) {
        this.excecutionDay = excecutionDay;
        return this;
    }

    public void setExcecutionDay(Integer excecutionDay) {
        this.excecutionDay = excecutionDay;
    }

    public BankAccount getBankAccountFrom() {
        return bankAccountFrom;
    }

    public StandingOrder bankAccountFrom(BankAccount bankAccount) {
        this.bankAccountFrom = bankAccount;
        return this;
    }

    public void setBankAccountFrom(BankAccount bankAccount) {
        this.bankAccountFrom = bankAccount;
    }

    public BankAccount getBankAccountTo() {
        return bankAccountTo;
    }

    public StandingOrder bankAccountTo(BankAccount bankAccount) {
        this.bankAccountTo = bankAccount;
        return this;
    }

    public void setBankAccountTo(BankAccount bankAccount) {
        this.bankAccountTo = bankAccount;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof StandingOrder)) {
            return false;
        }
        return id != null && id.equals(((StandingOrder) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "StandingOrder{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", user='" + getUser() + "'" +
            ", excecutionDay=" + getExcecutionDay() +
            "}";
    }
}
