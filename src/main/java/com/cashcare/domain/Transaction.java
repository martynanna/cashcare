package com.cashcare.domain;

import com.cashcare.config.Constants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * A Transaction.
 */
@Entity
@Table(name = "transaction")
public class Transaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "amount", nullable = false)
    private Float amount;

    @Pattern(regexp = Constants.USER_REGEX)
    @NotNull
    @Column(name = "jhi_user", nullable = false)
    private String user;

    @NotNull
    @Column(name = "timestamp", nullable = false)
    private LocalDateTime timestamp;

    @ManyToOne
    @JsonIgnoreProperties(value = "sentTransactions", allowSetters = true)
    private BankAccount bankAccountFrom;

    @ManyToOne
    @JsonIgnoreProperties(value = "receivedTransactions", allowSetters = true)
    private BankAccount bankAccountTo;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getAmount() {
        return amount;
    }

    public Transaction amount(Float amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getUser() {
        return user;
    }

    public Transaction user(String user) {
        this.user = user;
        return this;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public Transaction timestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public BankAccount getBankAccountFrom() {
        return bankAccountFrom;
    }

    public Transaction bankAccountFrom(BankAccount bankAccount) {
        this.bankAccountFrom = bankAccount;
        return this;
    }

    public void setBankAccountFrom(BankAccount bankAccount) {
        this.bankAccountFrom = bankAccount;
    }

    public BankAccount getBankAccountTo() {
        return bankAccountTo;
    }

    public Transaction bankAccountTo(BankAccount bankAccount) {
        this.bankAccountTo = bankAccount;
        return this;
    }

    public void setBankAccountTo(BankAccount bankAccount) {
        this.bankAccountTo = bankAccount;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Transaction)) {
            return false;
        }
        return id != null && id.equals(((Transaction) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Transaction{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", user='" + getUser() + "'" +
            ", timestamp='" + getTimestamp() + "'" +
            "}";
    }
}
