package com.cashcare.repository;

import com.cashcare.domain.BankAccount;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the BankAccount entity.
 */
@Repository
public interface BankAccountRepository extends JpaRepository<BankAccount, Long> {

    boolean existsByIban(String iban);

    Optional<BankAccount> getBankAccountByCustomerUserID(Long userID);

    List<BankAccount> findAllByArchived(@NotNull Boolean archived);

    Optional<BankAccount> getBankAccountByIban(@NotNull String iban);

}
