package com.cashcare.repository;

import com.cashcare.domain.Customer;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Customer entity.
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    @Query(value = "SELECT * FROM customer WHERE jhi_user_id=?1", nativeQuery = true)
    Optional<Customer> findCustomerByJhiUserId(Long id);

    @Query(value = "SELECT * FROM customer s JOIN jhi_user u on s.jhi_user_id = u.id where u.login = ?1", nativeQuery = true)
    Customer findCustomerByUsername(String username);

    List<Customer> findAllByBankAccountArchived(@NotNull Boolean bankAccount_archived);

}
