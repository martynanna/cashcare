package com.cashcare.repository;

import com.cashcare.domain.StandingOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the StandingOrder entity.
 */
@Repository
public interface StandingOrderRepository extends JpaRepository<StandingOrder, Long> {

    void deleteAllByBankAccountFrom_IdOrBankAccountTo_Id(Long bankAccountFrom_id, Long bankAccountTo_id);

    Optional<List<StandingOrder>> findAllByExcecutionDayEquals(@NotNull Integer excecutionDay);

    List<StandingOrder> findAllByBankAccountFrom_Customer_UserID(@NotNull Long bankAccountFrom_customer_userID);
}
