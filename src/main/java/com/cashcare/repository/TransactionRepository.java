package com.cashcare.repository;

import com.cashcare.domain.BankAccount;
import com.cashcare.domain.Transaction;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Spring Data  repository for the Transaction entity.
 */
@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    List<Transaction> findAllByBankAccountFrom_Customer_UserIDAndTimestampIsBetween(@NotNull Long bankAccountFrom_customer_userID, @NotNull LocalDateTime timestamp, @NotNull LocalDateTime timestamp2);
}
