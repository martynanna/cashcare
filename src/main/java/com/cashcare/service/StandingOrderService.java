package com.cashcare.service;

import com.cashcare.domain.BankAccount;
import com.cashcare.domain.StandingOrder;
import com.cashcare.domain.Transaction;
import com.cashcare.repository.BankAccountRepository;
import com.cashcare.repository.StandingOrderRepository;
import com.cashcare.repository.TransactionRepository;
import com.cashcare.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Service class for managing standing orders.
 */
@Service
@Transactional
@EnableScheduling
@Configuration
public class StandingOrderService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private final StandingOrderRepository standingOrderRepository;

    private final BankAccountRepository bankAccountRepository;

    private final TransactionRepository transactionRepository;

    public StandingOrderService(StandingOrderRepository standingOrderRepository, BankAccountRepository bankAccountRepository, TransactionRepository transactionRepository) {
        this.standingOrderRepository = standingOrderRepository;
        this.bankAccountRepository = bankAccountRepository;
        this.transactionRepository = transactionRepository;
    }

    @Scheduled(fixedDelay = 86400000L)
    void excecuteStandingOrder() {
        Optional<List<StandingOrder>> standingOrderListOptional = standingOrderRepository.findAllByExcecutionDayEquals(LocalDateTime.now().getDayOfMonth());
        if (standingOrderListOptional.isPresent()) {
            List<StandingOrder> standingOrderList = standingOrderListOptional.get();
            for (StandingOrder so : standingOrderList) {
                Transaction transaction = new Transaction();
                transaction.setBankAccountFrom(so.getBankAccountFrom());
                transaction.setBankAccountTo(so.getBankAccountTo());
                transaction.setAmount(so.getAmount());
                transaction.setTimestamp(LocalDateTime.now());
                transaction.setUser(so.getUser());

                BankAccount bankAccountFrom = bankAccountRepository.getOne(transaction.getBankAccountFrom().getId());
                BankAccount bankAccountTo = bankAccountRepository.getOne(transaction.getBankAccountTo().getId());
                if (bankAccountFrom.isArchived() || bankAccountTo.isArchived()) {
                    throw new BadRequestAlertException("A transaction receiver oder sender cannot be archived", "STANDING_ORDER", "archivedSenderOrReceiver");
                }
                if (bankAccountFrom.getBalance() - transaction.getAmount() > 0) {
                    bankAccountFrom.setBalance(bankAccountFrom.getBalance() - transaction.getAmount());
                    bankAccountRepository.save(bankAccountFrom);
                    bankAccountTo.setBalance(bankAccountTo.getBalance() + transaction.getAmount());
                    bankAccountRepository.save(bankAccountTo);
                    Transaction result = transactionRepository.save(transaction);
                }
            }
        }
    }

}
