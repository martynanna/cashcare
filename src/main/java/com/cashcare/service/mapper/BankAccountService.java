package com.cashcare.service.mapper;

import com.cashcare.domain.BankAccount;
import com.cashcare.domain.User;
import com.cashcare.repository.BankAccountRepository;
import com.cashcare.security.SecurityUtils;
import com.cashcare.service.UserService;
import com.cashcare.service.dto.UserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service class for managing bank accounts.
 */
@Service
@Transactional
public class BankAccountService {
    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private final BankAccountRepository bankAccountRepository;

    public BankAccountService(BankAccountRepository bankAccountRepository) {
        this.bankAccountRepository = bankAccountRepository;
    }

    public Optional<BankAccount> archiveBankAccount(Long userID) {
        log.debug("Archiving bankaccount for userID {}", userID);
        return bankAccountRepository.getBankAccountByCustomerUserID(userID)
            .map(bankAccount -> {
                System.out.println("################################### 1");
                bankAccount.setArchived(true);
                System.out.println("################################### 3");
                log.debug("Archived bankAccount: {}", bankAccount);
                System.out.println("################################### 2");
                return bankAccount;
            });
    }

    public Optional<BankAccount> getBankAccount(Long userID){
        return bankAccountRepository.getBankAccountByCustomerUserID(userID);
    }
}
