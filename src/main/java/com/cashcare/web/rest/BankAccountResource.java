package com.cashcare.web.rest;

import com.cashcare.domain.BankAccount;
import com.cashcare.domain.User;
import com.cashcare.repository.BankAccountRepository;
import com.cashcare.repository.StandingOrderRepository;
import com.cashcare.repository.UserRepository;
import com.cashcare.service.StandingOrderService;
import com.cashcare.service.UserService;
import com.cashcare.service.mapper.BankAccountService;
import com.cashcare.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.Random;

/**
 * REST controller for managing {@link com.cashcare.domain.BankAccount}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class BankAccountResource {

    private final Logger log = LoggerFactory.getLogger(BankAccountResource.class);

    private static final String ENTITY_NAME = "bankAccount";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BankAccountRepository bankAccountRepository;

    public BankAccountResource(BankAccountRepository bankAccountRepository) {
        this.bankAccountRepository = bankAccountRepository;
    }

    /**
     * {@code GET  /bank-accounts/:id} : get the "id" bankAccount.
     *
     * @param userID the id of the bankAccount to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the bankAccount, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/bank-accounts/{userID}")
    public ResponseEntity<BankAccount> getBankAccountByCustomerUserID(@PathVariable Long userID) {
        log.debug("REST request to get BankAccount : {}", userID);
        Optional<BankAccount> bankAccount = bankAccountRepository.getBankAccountByCustomerUserID(userID);
        return ResponseUtil.wrapOrNotFound(bankAccount);
    }

}
