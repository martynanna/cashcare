package com.cashcare.web.rest;

import com.cashcare.domain.BankAccount;
import com.cashcare.domain.StandingOrder;
import com.cashcare.repository.BankAccountRepository;
import com.cashcare.repository.StandingOrderRepository;
import com.cashcare.repository.UserRepository;
import com.cashcare.security.SecurityUtils;
import com.cashcare.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

/**
 * REST controller for managing {@link com.cashcare.domain.StandingOrder}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class StandingOrderResource {

    private final Logger log = LoggerFactory.getLogger(StandingOrderResource.class);

    private static final String ENTITY_NAME = "standingOrder";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final StandingOrderRepository standingOrderRepository;

    private final BankAccountRepository bankAccountRepository;

    private final UserRepository userRepository;

    public StandingOrderResource(StandingOrderRepository standingOrderRepository, BankAccountRepository bankAccountRepository, UserRepository userRepository) {
        this.bankAccountRepository = bankAccountRepository;
        this.standingOrderRepository = standingOrderRepository;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /standing-orders} : Create a new standingOrder.
     *
     * @param standingOrder the standingOrder to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new standingOrder, or with status {@code 400 (Bad Request)} if the standingOrder has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/standing-orders")
    public ResponseEntity<StandingOrder> createStandingOrder(@Valid @RequestBody StandingOrder standingOrder) throws URISyntaxException {
        log.debug("REST request to save StandingOrder : {}", standingOrder);
        if (standingOrder.getId() != null) {
            throw new BadRequestAlertException("A new standingOrder cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (standingOrder.getBankAccountFrom().getIban().equals(standingOrder.getBankAccountTo().getIban())) {
            throw new BadRequestAlertException("A transaction receiver cannot be the sender", ENTITY_NAME, "sameSenderAndSender");
        }
        if (standingOrder.getAmount() <= 0) {
            throw new BadRequestAlertException("A transaction amount must be positive", ENTITY_NAME, "negativeAmount");
        }
        if (standingOrder.getExcecutionDay() > 28 || standingOrder.getExcecutionDay() < 1) {
            throw new BadRequestAlertException("The excecution day must be between 1 and 28", ENTITY_NAME, "ecvecutionDayOutOfRange");
        }
        Optional<BankAccount> bankAccountFrom = bankAccountRepository.getBankAccountByIban(standingOrder.getBankAccountFrom().getIban());
        Optional<BankAccount> bankAccountTo = bankAccountRepository.getBankAccountByIban(standingOrder.getBankAccountTo().getIban());
        if (!bankAccountFrom.isPresent() || !bankAccountTo.isPresent()) {
            throw new BadRequestAlertException("The bank account was not found", ENTITY_NAME, "bankAccountNotFound");
        } else {
            standingOrder.setBankAccountTo(bankAccountTo.get());
            standingOrder.setBankAccountFrom(bankAccountFrom.get());
        }
        if (bankAccountFrom.get().isArchived() || bankAccountTo.get().isArchived()) {
            throw new BadRequestAlertException("A transaction receiver oder sender cannot be archived", ENTITY_NAME, "archivedSenderOrReceiver");
        }
        StandingOrder result = standingOrderRepository.save(standingOrder);
        return ResponseEntity.created(new URI("/api/standing-orders/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /standing-orders} : Updates an existing standingOrder.
     *
     * @param standingOrder the standingOrder to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated standingOrder,
     * or with status {@code 400 (Bad Request)} if the standingOrder is not valid,
     * or with status {@code 500 (Internal Server Error)} if the standingOrder couldn't be updated.
     */
    @PutMapping("/standing-orders")
    public ResponseEntity<StandingOrder> updateStandingOrder(@Valid @RequestBody StandingOrder standingOrder) {
        log.debug("REST request to update StandingOrder : {}", standingOrder);
        if (standingOrder.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        StandingOrder standingOrderOriginal = standingOrderRepository.getOne(standingOrder.getId());

        standingOrder.setBankAccountTo(standingOrderOriginal.getBankAccountTo());
        standingOrder.setBankAccountFrom(standingOrderOriginal.getBankAccountFrom());
        if (standingOrder.getExcecutionDay() > 28 || standingOrder.getExcecutionDay() < 1) {
            throw new BadRequestAlertException("The excecution day must be between 1 and 28", ENTITY_NAME, "ecvecutionDayOutOfRange");
        }
        StandingOrder result = standingOrderRepository.save(standingOrder);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, standingOrder.getId().toString()))
            .body(result);
    }


    /**
     * {@code GET  /standing-orders} : Gets all existing orders.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the list of standing orders,
     * or with status {@code 500 (Internal Server Error)} if the standingOrders couldn't be retreived.
     */
    @GetMapping("/standing-orders")
    public ResponseEntity<?> getStandingOrdersOfCurrentCustomer() {
        String login = "";
        if (SecurityUtils.getCurrentUserLogin().isPresent()) {
            login = SecurityUtils.getCurrentUserLogin().get();
        }
        log.debug("REST request to retreive all StandingOrders for Account : {}", login);
        final Object[] standingOrderList = new Object[1];
        SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(user -> {
                standingOrderList[0] = standingOrderRepository.findAllByBankAccountFrom_Customer_UserID(user.getId());
            });
        return ResponseEntity.ok()
            .body(standingOrderList[0]);
    }

    /**
     * {@code DELETE  /standing-orders/{standingOrderID}} : Deletes a standing order.
     *
     * @return the status {@code 200 (OK)},
     * or with status {@code 500 (Internal Server Error)} if the standingOrders couldn't be retreived.
     */
    @DeleteMapping("/standing-orders/{standingOrderID}")
    public ResponseEntity<Void> deleteAStandingOrder(@PathVariable Long standingOrderID) {
        String login = "";
        if (SecurityUtils.getCurrentUserLogin().isPresent()) {
            login = SecurityUtils.getCurrentUserLogin().get();
        }
        log.debug("REST request to retreive all StandingOrders for Account : {}", login);
        SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(user -> {
                StandingOrder standingOrder = standingOrderRepository.getOne(standingOrderID);
                if (standingOrder.getBankAccountFrom().getCustomer().getUserID().equals(user.getId())) {
                    standingOrderRepository.delete(standingOrder);
                }
            });
        return ResponseEntity.noContent().headers(HeaderUtil.createAlert(applicationName, "standingOrderManagement.deleted", login)).build();
    }


}
