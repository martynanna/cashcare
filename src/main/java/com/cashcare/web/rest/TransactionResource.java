package com.cashcare.web.rest;

import com.cashcare.domain.BankAccount;
import com.cashcare.domain.Transaction;
import com.cashcare.domain.User;
import com.cashcare.repository.BankAccountRepository;
import com.cashcare.repository.TransactionRepository;
import com.cashcare.repository.UserRepository;
import com.cashcare.security.SecurityUtils;
import com.cashcare.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.cashcare.domain.Transaction}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class TransactionResource {

    private final Logger log = LoggerFactory.getLogger(TransactionResource.class);

    private static final String ENTITY_NAME = "transaction";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TransactionRepository transactionRepository;

    private final BankAccountRepository bankAccountRepository;

    private final UserRepository userRepository;

    public TransactionResource(TransactionRepository transactionRepository, BankAccountRepository bankAccountRepository, UserRepository userRepository) {
        this.bankAccountRepository = bankAccountRepository;
        this.transactionRepository = transactionRepository;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /transactions} : Create a new transaction.
     *
     * @param transaction the transaction to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new transaction, or with status {@code 400 (Bad Request)} if the transaction has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/transactions")
    public ResponseEntity<Transaction> createTransaction(@Valid @RequestBody Transaction transaction) throws URISyntaxException {
        log.debug("REST request to save Transaction : {}", transaction);
        if (transaction.getId() != null) {
            throw new BadRequestAlertException("A new transaction cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (transaction.getBankAccountFrom().getIban().equals(transaction.getBankAccountTo().getIban())) {
            throw new BadRequestAlertException("A transaction receiver cannot be the sender", ENTITY_NAME, "sameSenderAndReceiver");
        }
        if (transaction.getAmount() <= 0) {
            throw new BadRequestAlertException("A transaction amount must be positive", ENTITY_NAME, "negativeAmount");
        }
        Optional<BankAccount> bankAccountFrom = bankAccountRepository.getBankAccountByIban(transaction.getBankAccountFrom().getIban());
        Optional<BankAccount> bankAccountTo = bankAccountRepository.getBankAccountByIban(transaction.getBankAccountTo().getIban());
        if (!bankAccountFrom.isPresent() || !bankAccountTo.isPresent()){
            throw new BadRequestAlertException("The bank account was not found", ENTITY_NAME, "bankAccountNotFound");
        }
        if (bankAccountFrom.get().isArchived() || bankAccountTo.get().isArchived()) {
            throw new BadRequestAlertException("A transaction receiver oder sender cannot be archived", ENTITY_NAME, "archivedSenderOrReceiver");
        }
        if (bankAccountFrom.get().getBalance() - transaction.getAmount() > 0) {
            bankAccountFrom.get().setBalance(bankAccountFrom.get().getBalance() - transaction.getAmount());
            bankAccountRepository.save(bankAccountFrom.get());
            bankAccountTo.get().setBalance(bankAccountTo.get().getBalance() + transaction.getAmount());
            bankAccountRepository.save(bankAccountTo.get());
            transaction.setBankAccountTo(bankAccountTo.get());
            transaction.setBankAccountFrom(bankAccountFrom.get());
        } else {
            throw new BadRequestAlertException("Balance too low", ENTITY_NAME, "lowBalance");
        }
        Transaction result = transactionRepository.save(transaction);
        return ResponseEntity.created(new URI("/api/transactions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }


    /**
     * {@code GET  /transactions} : get all the transactions within timestamps.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of transactions in body.
     */
    @GetMapping("/transactions")
    public List<Transaction> getAllTransactions(@RequestParam LocalDateTime from, @RequestParam LocalDateTime to) {
        log.debug("REST request to get Transactions");
        Optional<User> user = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneWithAuthoritiesByLogin);
        if (user.isPresent()) {
            return transactionRepository.findAllByBankAccountFrom_Customer_UserIDAndTimestampIsBetween(user.get().getId(), from, to);
        } else {
            throw new IllegalArgumentException("User not found");
        }
    }

}
