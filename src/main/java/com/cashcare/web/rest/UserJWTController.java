package com.cashcare.web.rest;

import com.cashcare.domain.PersistentAuditEvent;
import com.cashcare.repository.PersistenceAuditEventRepository;
import com.cashcare.security.SecurityUtils;
import com.cashcare.security.jwt.JWTFilter;
import com.cashcare.security.jwt.TokenProvider;
import com.cashcare.web.rest.vm.LoginVM;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.jsonwebtoken.MalformedJwtException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Instant;
import java.util.HashMap;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
public class UserJWTController {

    private final TokenProvider tokenProvider;

    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    private final PersistenceAuditEventRepository persistenceAuditEventRepository;

    public UserJWTController(TokenProvider tokenProvider, AuthenticationManagerBuilder authenticationManagerBuilder, PersistenceAuditEventRepository persistenceAuditEventRepository) {
        this.tokenProvider = tokenProvider;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.persistenceAuditEventRepository = persistenceAuditEventRepository;
    }

    @PostMapping("/authenticate")
    public ResponseEntity<JWTToken> authorize(@Valid @RequestBody LoginVM loginVM) {

        UsernamePasswordAuthenticationToken authenticationToken =
            new UsernamePasswordAuthenticationToken(loginVM.getUsername(), loginVM.getPassword());

        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        //boolean rememberMe = (loginVM.isRememberMe() == null) ? false : loginVM.isRememberMe();
        boolean rememberMe = loginVM.isRememberMe() != null && loginVM.isRememberMe();
        String jwt = tokenProvider.createToken(authentication, rememberMe);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
        return new ResponseEntity<>(new JWTToken(jwt), httpHeaders, HttpStatus.OK);
    }

    @PostMapping("/logout")
    public void logout(){
        PersistentAuditEvent pae = new PersistentAuditEvent();
        pae.setAuditEventType("LOGOUT");
        pae.setAuditEventDate(Instant.now());
        if(SecurityUtils.getCurrentUserLogin().isPresent()){
            pae.setPrincipal(SecurityUtils.getCurrentUserLogin().get());
        } else{
            pae.setPrincipal("no Principal");
            HashMap<String, String> map = new HashMap<>();
            map.put("type", MalformedJwtException.class.getCanonicalName());
            map.put("message", "no Principal found");
            pae.setData(map);
        }
        persistenceAuditEventRepository.save(pae);
    }

    /**
     * Object to return as body in JWT Authentication.
     */
    static class JWTToken {

        private String idToken;

        JWTToken(String idToken) {
            this.idToken = idToken;
        }

        @JsonProperty("id_token")
        String getIdToken() {
            return idToken;
        }

        void setIdToken(String idToken) {
            this.idToken = idToken;
        }
    }
}
