package com.cashcare.web.rest;

import com.cashcare.config.Constants;
import com.cashcare.domain.BankAccount;
import com.cashcare.domain.Customer;
import com.cashcare.domain.PersistentAuditEvent;
import com.cashcare.domain.User;
import com.cashcare.repository.*;
import com.cashcare.security.AuthoritiesConstants;
import com.cashcare.security.SecurityUtils;
import com.cashcare.service.UserService;
import com.cashcare.service.dto.UserDTO;
import com.cashcare.service.mapper.BankAccountService;
import com.cashcare.web.rest.errors.BadRequestAlertException;
import com.cashcare.web.rest.errors.EmailAlreadyUsedException;
import com.cashcare.web.rest.errors.InvalidInput;
import com.cashcare.web.rest.errors.LoginAlreadyUsedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * REST controller for managing users.
 * <p>
 * This class accesses the {@link User} entity, and needs to fetch its collection of authorities.
 * <p>
 * For a normal use-case, it would be better to have an eager relationship between User and Authority,
 * and send everything to the client side: there would be no View Model and DTO, a lot less code, and an outer-join
 * which would be good for performance.
 * <p>
 * We use a View Model and a DTO for 3 reasons:
 * <ul>
 * <li>We want to keep a lazy association between the user and the authorities, because people will
 * quite often do relationships with the user, and we don't want them to get the authorities all
 * the time for nothing (for performance reasons). This is the #1 goal: we should not impact our users'
 * application because of this use-case.</li>
 * <li> Not having an outer join causes n+1 requests to the database. This is not a real issue as
 * we have by default a second-level cache. This means on the first HTTP call we do the n+1 requests,
 * but then all authorities come from the cache, so in fact it's much better than doing an outer join
 * (which will get lots of data from the database, for each HTTP call).</li>
 * <li> As this manages users, for security reasons, we'd rather have a DTO layer.</li>
 * </ul>
 * <p>
 * Another option would be to have a specific JPA entity graph to handle this case.
 */
@RestController
@RequestMapping("/api")
public class UserResource {
    private static final List<String> ALLOWED_ORDERED_PROPERTIES = Collections.unmodifiableList(Arrays.asList("id", "login", "firstName", "lastName", "email", "activated", "langKey"));

    private static class UserResourceException extends BadRequestAlertException {
        private UserResourceException(String message){
            super(message, "USER", "400");
        }
    }

    private final Logger log = LoggerFactory.getLogger(UserResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserService userService;

    private final UserRepository userRepository;

    private final BankAccountService bankAccountService;

    private final StandingOrderRepository standingOrderRepository;

    private final CustomerRepository customerRepository;

    private final BankAccountRepository bankAccountRepository;

    private final PersistenceAuditEventRepository persistenceAuditEventRepository;

    public UserResource(UserService userService, UserRepository userRepository, BankAccountService bankAccountService, StandingOrderRepository standingOrderRepository, CustomerRepository customerRepository, BankAccountRepository bankAccountRepository, PersistenceAuditEventRepository persistenceAuditEventRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
        this.bankAccountService = bankAccountService;
        this.standingOrderRepository = standingOrderRepository;
        this.customerRepository = customerRepository;
        this.bankAccountRepository = bankAccountRepository;
        this.persistenceAuditEventRepository = persistenceAuditEventRepository;
    }

    /**
     * {@code POST  /users}  : Creates a new user.
     * <p>
     * Creates a new user if the login and email are not already used, and sends an
     * mail with an activation link.
     * The user needs to be activated on creation.
     *
     * @param object with parameters to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new user, or with status {@code 400 (Bad Request)} if the login or email is already in use.
     * @throws BadRequestAlertException {@code 400 (Bad Request)} if the login or email is already in use.
     */
    @PostMapping("/users")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<String> createUser(@Valid @RequestBody HashMap<String, String> object) {
        log.debug("REST request to save User : {}", object);

        if (object.get("email") == null || object.get("email").trim().isEmpty() || object.get("firstName") == null || object.get("firstName").trim().isEmpty() ||
            object.get("lastName") == null || object.get("lastName").trim().isEmpty() ||
            !(object.get("authorities").equals(AuthoritiesConstants.USER) || object.get("authorities").equals(AuthoritiesConstants.ADMIN) || object.get("password") == null)) {
            throw new UserResourceException("Not enough information to create a new user");
        }

        if (object.get("authorities").equals(AuthoritiesConstants.USER)) {
            if ((object.get("city") == null || object.get("city").trim().isEmpty() ||
                object.get("country") == null || object.get("country").trim().isEmpty() || object.get("house") == null || object.get("house").trim().isEmpty() ||
                object.get("phone") == null || object.get("phone").trim().isEmpty() || object.get("postalCode") == null || object.get("postalCode").trim().isEmpty() ||
                object.get("street") == null || object.get("street").trim().isEmpty())) {
                throw new UserResourceException("Not enough information to create a new customer");
            }
        }

        if(object.get("authorities").equals(AuthoritiesConstants.ADMIN) && (!(object.get("firstName").matches(Constants.LETTER_REGEX)) || !(object.get("lastName").matches(Constants.LETTER_REGEX)) ||
            !(object.get("password").matches(Constants.USER_BCRYPT_REGEX)))) {
            throw new InvalidInput();
        }

        if(object.get("authorities").equals(AuthoritiesConstants.USER) && (!(object.get("firstName").matches(Constants.LETTER_REGEX)) || !(object.get("lastName").matches(Constants.LETTER_REGEX)) || !(object.get("password").matches(Constants.USER_BCRYPT_REGEX)) ||
            !(object.get("city").matches(Constants.LETTER_REGEX)) || !(object.get("country").matches(Constants.LETTER_REGEX)) || !(object.get("house").matches(Constants.HOUSE_REGEX)) ||
            !(object.get("phone").matches(Constants.PHONE_REGEX)) || !(object.get("postalCode").matches(Constants.POSTAL_REGEX)) || !(object.get("street").matches(Constants.LETTER_REGEX)))) {
             throw new InvalidInput();
        }

        if (userRepository.findOneByEmailIgnoreCase(object.get("email").toLowerCase()).isPresent()) {
            throw new EmailAlreadyUsedException();
        } else {
            UserDTO user = new UserDTO();
            user.setAuthorities(new HashSet<String>() {{
                add(object.get("authorities"));
            }});
            user.setBirthday(LocalDate.parse(object.get("birthday"), DateTimeFormatter.ISO_DATE));
            user.setEmail(object.get("email"));
            user.setFirstName(object.get("firstName"));
            user.setLastName(object.get("lastName"));
            user.setPassword(object.get("password"));
            User newUser = userService.createUser(user);

            if (object.get("authorities").equals(AuthoritiesConstants.USER)) {
                Customer customer = new Customer();
                customer.setCity(object.get("city"));
                customer.setCountry(object.get("country"));
                customer.setHouse(object.get("house"));
                customer.setPhone(object.get("phone"));
                customer.setPostalCode(object.get("postalCode"));
                customer.setStreet(object.get("street"));
                customer.setUserID(newUser.getId());
                Customer newCustomer = customerRepository.save(customer);

                BankAccount bankAccount = new BankAccount();
                bankAccount.setCustomer(newCustomer);

                Random random = new Random();
                StringBuilder iban;
                do {
                    iban = new StringBuilder();
                    for (int i = 0; i < 2; i++) {
                        iban.append(random.nextInt(89999) + 10000);
                    }
                } while (bankAccountRepository.existsByIban(iban.toString()));
                bankAccount.setIban(iban.toString());
                BankAccount result = bankAccountRepository.save(bankAccount);
            }
            PersistentAuditEvent pae = new PersistentAuditEvent();
            pae.setAuditEventType("NEW USER " + newUser.getLogin() + " WITH ROLE " + newUser.getAuthorities());
            pae.setAuditEventDate(Instant.now());
            if(SecurityUtils.getCurrentUserLogin().isPresent()){
                pae.setPrincipal(SecurityUtils.getCurrentUserLogin().get());
            }
            persistenceAuditEventRepository.save(pae);
            return ResponseEntity.ok(newUser.getLogin());
        }
    }

    /**
     * {@code PUT /users} : Updates an existing User.
     *
     * @throws EmailAlreadyUsedException {@code 400 (Bad Request)} if the email is already in use.
     * @throws LoginAlreadyUsedException {@code 400 (Bad Request)} if the login is already in use.
     */
    @PutMapping("/users/{login}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public void updateUser(@Valid @RequestBody HashMap<String, String> object, @PathVariable String login) {
        log.debug("REST request to update User : {}", object);

        Object[] objects = getAnAccount(login);
        UserDTO user = (UserDTO)objects[0];

        if(object.containsKey("firstName")){
            user.setFirstName(object.get("firstName"));
        }
        if(object.containsKey("lastName")){
            user.setLastName(object.get("lastName"));
        }
        if(object.containsKey("email")){
            user.setEmail(object.get("email"));
        }

        BankAccount bankAccount;
        Customer customer;
        if(objects[1] != null){
            bankAccount = (BankAccount)objects[1];
            customer = bankAccount.getCustomer();
            if(object.containsKey("country")){
                customer.setCountry(object.get("country"));
            }
            if(object.containsKey("city")){
                customer.setCity(object.get("city"));
            }
            if(object.containsKey("street")){
                customer.setStreet(object.get("street"));
            }
            if(object.containsKey("house")){
                customer.setHouse(object.get("house"));
            }
            if(object.containsKey("postalCode")){
                customer.setPostalCode(object.get("postalCode"));
            }
            if(object.containsKey("phone")){
                customer.setPhone(object.get("phone"));
            }

            customerRepository.save(customer);
        }
        userService.updateUser(user);
    }



    /**
     * {@code GET /users} : get all users.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body all users.
     */
    @GetMapping("/users")
    public ResponseEntity<?> getAllUsers(@RequestParam String role) {

        if (!role.equals(AuthoritiesConstants.ADMIN) && !role.equals(AuthoritiesConstants.USER)){
            return ResponseEntity.badRequest().body("Cannot understand role");
        }
        List<User> users = userRepository.finByRoleAndArchived(role, false);

        if (role.equals("ROLE_ADMIN")){
            return ResponseEntity.ok(users);
        }
        else{
            ArrayList<Object[]> result = new ArrayList<>();
            HashMap<Long, BankAccount> bankAccountHashMap =  new HashMap<>();
            List<BankAccount> bankAccounts = bankAccountRepository.findAllByArchived(false);
            for (BankAccount bankAccount : bankAccounts){
                bankAccountHashMap.put(bankAccount.getCustomer().getUserID(), bankAccount);
            }
            for (User user : users){
                result.add(new Object[]{user, bankAccountHashMap.get(user.getId())});
            }
            return ResponseEntity.ok(result);
        }
    }

    /**
     * {@code GET  /archive} : archive the registered user.
     *
     * @param userID the activation key.
     * @throws RuntimeException {@code 500 (Internal Server Error)} if the user couldn't be archived.
     */
    @GetMapping("/archive")
    public void archiveAccount(@RequestParam(value = "userID") Long userID) {
        Optional<User> user = userService.archiveAccount(userID);
        if (!user.isPresent()) {
            throw new IllegalArgumentException("No user was found for this userID");
        }
        Optional<BankAccount> bankAccount = bankAccountService.archiveBankAccount(userID);
        bankAccount.ifPresent(account -> standingOrderRepository.deleteAllByBankAccountFrom_IdOrBankAccountTo_Id(account.getId(), account.getId()));
    }

    /**
     * {@code GET  /account} : get the current account.
     *
     * @return the current user.
     * @throws RuntimeException {@code 500 (Internal Server Error)} if the user couldn't be returned.
     */
    @GetMapping("/account")
    public Object[] getCurrentAccount() {
        Object[] ob = new Object[2];
        UserDTO user = userService.getUserWithAuthorities()
            .map(UserDTO::new)
            .orElseThrow(() -> new UserResource.UserResourceException("User could not be found"));
        ob[0] = user;
        if (user.getAuthorities().contains(AuthoritiesConstants.USER)) {
            Optional<BankAccount> bankAccount = bankAccountService.getBankAccount(user.getId());
            if (bankAccount.isPresent()) {
                ob[1] = bankAccount.get();
            } else {
                throw new UserResourceException("Customer or bank account not found");
            }
        }
        return ob;
    }


    /**
     * {@code GET  /account} : get the current account.
     *
     * @return the current user.
     * @throws RuntimeException {@code 500 (Internal Server Error)} if the user couldn't be returned.
     */
    @GetMapping("/account/{login}")
    public Object[] getAnAccount(@PathVariable String login) {
        Object[] ob = new Object[2];
        UserDTO user = userService.getUserWithAuthoritiesByLogin(login)
            .map(UserDTO::new)
            .orElseThrow(() -> new UserResource.UserResourceException("User could not be found"));
        ob[0] = user;
        if (user.getAuthorities().contains(AuthoritiesConstants.USER)) {
            Optional<BankAccount> bankAccount = bankAccountService.getBankAccount(user.getId());
            if (bankAccount.isPresent()) {
                ob[1] = bankAccount.get();
            } else {
                throw new UserResourceException("Customer or bank account not found");
            }
        }
        return ob;
    }
}
