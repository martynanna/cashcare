package com.cashcare.web.rest.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class InvalidInput extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;

    public InvalidInput() { super(ErrorConstants.FALSE_INPUT_VALIDATION_TYPE, "Invalid Input", Status.BAD_REQUEST);}
}
