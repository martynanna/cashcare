/**
 * View Models used by Spring MVC REST controllers.
 */
package com.cashcare.web.rest.vm;
