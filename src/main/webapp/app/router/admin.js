import { Authority } from '@/shared/security/authority';

const AdminAccount = () => import('@/views/admin/AdminAccount.vue');
const CustomerOverview = () => import('@/views/admin/CustomerOverview.vue');
const LogFiles = () => import('@/views/admin/LogFiles.vue');
const AdminOverview = () => import('@/views/admin/AdminOverview.vue');
const CreateCustomer = () => import('@/views/admin/CreateCustomer.vue');

export default [
  {
    path: '/admin/customerOverview',
    name: 'CustomerOverview',
    component: CustomerOverview,
    meta: { requireAuth: true, authorities: [Authority.ADMIN] },
  },
  {
    path: '/admin/account',
    name: 'AdminAccount',
    component: AdminAccount,
    meta: { requireAuth: true, authorities: [Authority.ADMIN] },
  },
  {
    path: '/admin/log',
    name: 'LogFiles',
    component: LogFiles,
    meta: { requireAuth: true, authorities: [Authority.ADMIN] },
  },
  {
    path: '/admin/adminOverview',
    name: 'AdminOverview',
    component: AdminOverview,
    meta: { requireAuth: true, authorities: [Authority.ADMIN] },
  },
  {
    path: '/admin/create/cust',
    name: 'CreateCustomer',
    component: CreateCustomer,
    meta: { requireAuth: true, authorities: [Authority.ADMIN] },
  },
];
