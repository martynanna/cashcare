import { Authority } from '@/shared/security/authority';

const TransactionHistory = () => import('@/views/customer/TransactionHistory.vue');
const NewTransaction = () => import('@/views/customer/NewTransaction.vue');
const CustomerAccount = () => import('@/views/customer/CustomerAccount.vue');
const StandingTransactions = () => import('@/views/customer/StandingTransactions.vue');

export default [
  {
    path: '/cust/transaction/history',
    name: 'TransactionHistory',
    component: TransactionHistory,
    meta: { requireAuth: true, authorities: [Authority.ADMIN] },
  },
  {
    path: '/cust/transaction/new',
    name: 'NewTransaction',
    component: NewTransaction,
    meta: { requireAuth: true, authorities: [Authority.ADMIN] },
  },
  {
    path: '/cust/account',
    name: 'CustomerAccount',
    component: CustomerAccount,
    meta: { requireAuth: true, authorities: [Authority.ADMIN] },
  },
  {
    path: '/cust/transaction/standing',
    name: 'StandingTransactions',
    component: StandingTransactions,
    meta: { requireAuth: true, authorities: [Authority.ADMIN] },
  },
];
