import Vue from 'vue';
import Component from 'vue-class-component';
import Router from 'vue-router';
import admin from '@/router/admin.js';
import customer from '@/router/customer.js';

Component.registerHooks([
  'beforeRouteEnter',
  'beforeRouteLeave',
  'beforeRouteUpdate', // for vue-router 2.2+
]);

const Login = () => import('@/views/core/login.vue');
const NotFound = () => import('@/views/core/NotFound.vue');

Vue.use(Router);

// prettier-ignore
export default new Router({
  mode: 'history',
  beforeRouteEnter(to, from, next) {

    console.log("REDIRECT");
    next();

  },
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Login
    },
    {
      path: '*',
      name: '404',
      component: NotFound
    },
    ...admin,
    ...customer,
  ]
});
