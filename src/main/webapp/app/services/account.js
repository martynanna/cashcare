import axios from 'axios';

const URL_BASE = process.env.NODE_ENV === 'production' ? window.location.protocol + '//' + window.location.host : 'http://localhost:8080';

const URL_API = URL_BASE + '/api';
const URL_ACC = URL_API + '/account';
const URL_USER = URL_API + '/users';
const URL_ARC = URL_API + '/archive?userID=';

/**
 * Performs an ajax get request using axios
 *
 * @param {string} url - request url
 * @param {string} [token] - the token
 * @returns {Promise} - the Promise containing the result of the HTTP request
 */
function get(url, token) {
  const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };

  if (token) {
    headers.Authorization = 'Bearer ' + token;
  }

  const config = {
    headers,
  };

  return axios.get(url, config);
}

/**
 * Performs an ajax put request using axios
 *
 * @param {string} url - request url
 * @param {Object} [content={}] - request body data in json format
 * @param {string} [token] - the token
 * @returns {Promise} - the Promise containing the result of the HTTP request
 */
function put(url, content = {}, token) {
  const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };

  if (token) {
    headers.Authorization = 'Bearer ' + token;
  }

  const config = {
    headers,
  };

  return axios.put(url, content, config);
}

/**
 * Get the information of the user with the given jwt
 *
 * @param {string} jwt - the identification
 * @returns {Promise} - the Promise containing the result of the HTTP request
 */
export async function getCurrentAccount(jwt) {
  return await get(URL_ACC, jwt);
}

/**
 *  Get the information of all customers
 * @param jwt - the identification
 * @returns {Promise} - the Promise containing the result of the HTTP request
 */
export async function getAllCustomers(jwt) {
  const URL_GET_CUS = URL_USER + '?role=ROLE_USER';
  return await get(URL_GET_CUS, jwt);
}

/**
 *  Get the information of all admins
 * @param jwt - the identification
 * @returns {Promise} - the Promise containing the result of the HTTP request
 */
export async function getAllAdmins(jwt) {
  const URL_GET_ADMIN = URL_USER + '?role=ROLE_ADMIN';
  return await get(URL_GET_ADMIN, jwt);
}

/**
 * Updates the information of an admin#
 *
 * @param username
 * @param {string} jwt - the identification
 * @param {string} firstName
 * @param {string} lastName
 * @param {string} email
 * @returns {Promise<*>} - the Promise containing the result of the HTTP request
 */
export async function updateAdmin(username, jwt, firstName, lastName, email) {
  const updateRequest = {
    firstName,
    lastName,
    email,
  };

  const URL_USER_NAME = URL_USER + '/' + username;
  return await put(URL_USER_NAME, updateRequest, jwt);
}

/**
 *
 * @param username
 * @param {string} jwt - the identification
 * @param firstName
 * @param lastName
 * @param email
 * @param street
 * @param house
 * @param postalCode
 * @param city
 * @param country
 * @param phone
 * @returns {Promise<*>} - the Promise containing the result of the HTTP request
 */
export async function updateCustomer(username, jwt, firstName, lastName, email, street, house, postalCode, city, country, phone) {
  const updateRequest = {
    firstName,
    lastName,
    email,
    street,
    house,
    postalCode,
    city,
    country,
    phone,
  };

  const URL_USER_NAME = URL_USER + '/' + username;
  return await put(URL_USER_NAME, updateRequest, jwt);
}

/**
 * Archives existing User
 *
 * @param jwt - the identification
 * @param id - id of the user to be archived
 * @returns {Promise<*>} - the Promise containing the result of the HTTP request
 */
export async function archiveUser(jwt, id) {
  const URL_ARC_USER = URL_ARC + id;
  return await get(URL_ARC_USER, jwt);
}
