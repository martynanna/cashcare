import axios from 'axios';

const URL_BASE = process.env.NODE_ENV === 'production' ? window.location.protocol + '//' + window.location.host : 'http://localhost:8080';

const URL_AUTH = URL_BASE + '/api/authenticate';

/**
 * Performs an ajax post request using axios
 *
 * @param {string} url - request url
 * @param {Object} [content={}] - request body data in json format
 * @param {string} [token] - the token
 * @returns {Promise} - the Promise containing the result of the HTTP request
 */
function post(url, content = {}, token) {
  const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };

  if (token) {
    headers.Authorization = 'Bearer ' + token;
  }

  const config = {
    headers,
  };

  return axios.post(url, content, config);
}

/**
 * Performs a login on the backend, request body data in json format, eg. {"authenticationRequest":{}}
 *
 * @param {string} username - the username
 * @param {string} password - the password
 * @returns {Promise} - the Promise containing the result of the HTTP request
 */
export async function doLogin(username, password) {
  const authenticationRequest = {
    username,
    password,
  };
  return await post(URL_AUTH, authenticationRequest);
}
