import axios from 'axios';

const URL_BASE = process.env.NODE_ENV === 'production' ? window.location.protocol + '//' + window.location.host : 'http://localhost:8080';

const URL_USER = URL_BASE + '/api/users';

/**
 * Performs an ajax post request using axios
 *
 * @param {string} url - request url
 * @param {Object} [content={}] - request body data in json format
 * @param {string} [token] - the token
 * @returns {Promise} - the Promise containing the result of the HTTP request
 */
function post(url, content = {}, token) {
  const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };

  if (token) {
    headers.Authorization = 'Bearer ' + token;
  }

  const config = {
    headers,
  };

  return axios.post(url, content, config);
}

/**
 * Create a customer
 *
 * @param jwt - the identification
 * @param firstName
 * @param lastName
 * @param email
 * @param birthday
 * @param password - encrypted with bcrypt
 * @param street
 * @param house
 * @param postalCode
 * @param city
 * @param country
 * @param phone
 * @returns {Promise<*>} - the Promise containing the result of the HTTP request
 */
export async function createCustomer(jwt, firstName, lastName, email, birthday, password, street, house, postalCode, city, country, phone) {
  const createRequest = {
    authorities: 'ROLE_USER',
    firstName,
    lastName,
    email,
    birthday,
    password,
    street,
    house,
    postalCode,
    city,
    country,
    phone,
  };

  return await post(URL_USER, createRequest, jwt);
}

/**
 * Create an Admin
 *
 * @param jwt - the identification
 * @param firstName
 * @param lastName
 * @param email
 * @param birthday
 * @param password - encrypted with bcrypt
 * @returns {Promise<*>} - the Promise containing the result of the HTTP request
 */
export async function createAdmin(jwt, firstName, lastName, email, birthday, password) {
  const createRequest = {
    authorities: 'ROLE_ADMIN',
    firstName,
    lastName,
    email,
    birthday,
    password,
  };

  return await post(URL_USER, createRequest, jwt);
}
