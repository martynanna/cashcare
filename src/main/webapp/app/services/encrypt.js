import bcrypt from 'bcryptjs';

/**
 * Encodes the given password with 10 rounds of bcrypt
 * @param password
 * @returns {Promise<*>}
 */
export async function encryptPassword(password) {
  const salt = bcrypt.genSaltSync(10);
  return bcrypt.hashSync(password, salt);
}
