/**
 *  Generates a radom password containing only upper case letters
 *
 * @returns {Promise<string>}
 */
export async function generateRadomPassword() {
  let password = '';
  let charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

  for (var i = 0; i < 8; ++i) {
    password += charset.charAt(Math.floor(Math.random() * charset.length) + 1);
  }
  return password;
}
