import axios from 'axios';

const URL_BASE = process.env.NODE_ENV === 'production' ? window.location.protocol + '//' + window.location.host : 'http://localhost:8080';

const URL_AUD = URL_BASE + '/management/audits';

/**
 * Performs an ajax get request using axios
 *
 * @param {string} url - request url
 * @param {string} [token] - the token
 * @returns {Promise} - the Promise containing the result of the HTTP request
 */
function get(url, token) {
  const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };

  if (token) {
    headers.Authorization = 'Bearer ' + token;
  }

  const config = {
    headers,
  };

  return axios.get(url, config);
}

/**
 *
 * @param jwt - the identification
 * @param from
 * @param to
 * @returns {Promise<*>} - the Promise containing the result of the HTTP request
 */
export async function getAuditLogs(jwt, from, to) {
  const URL_GET_AUD = URL_AUD + '?fromDate=' + from + '&toDate=' + to;
  return await get(URL_GET_AUD, jwt);
}
