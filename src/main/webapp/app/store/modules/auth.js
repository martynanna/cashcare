import { doLogin } from '@/services/authentification';

const state = {
  jwt: null,
  sessionTimeout: null,
};

const actions = {
  // TODO Delete console.logs
  async login({ commit }, loginData) {
    commit('setJWT', null);

    try {
      if (!loginData.username) return;
      if (!loginData.password) return;
      const response = await doLogin(loginData.username, loginData.password);

      commit('setJWT', response.data.id_token);
      console.log('RESPONSE STORE');
      console.log(response);
      return response;
    } catch (e) {
      console.log('ERROR');
      console.log(e.response);
      if (e.response.data.status === 400) {
        return '400';
      }
    }
  },
  timingOut({ commit }) {
    commit('setJWT', null);
    commit('setSessionTimeout', true);
  },
  loggingOut({ commit }) {
    commit('setJWT', null);
  },
};

const getters = {
  jwt(state) {
    if (!state.jwt) {
      return;
    }
    return state.jwt;
  },
  sessionTimeout(state) {
    if (!state.sessionTimeout) {
      return;
    }
    return state.sessionTimeout;
  },
};

const mutations = {
  setJWT(state, jwt) {
    state.jwt = jwt;
  },
  setSessionTimeout(state, sessionTimeout) {
    state.sessionTimeout = sessionTimeout;
  },
};

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations,
};
