package com.cashcare.web.rest;

import com.cashcare.CashcareApp;
import com.cashcare.domain.Authority;
import com.cashcare.domain.User;
import com.cashcare.repository.UserRepository;
import com.cashcare.security.AuthoritiesConstants;
import com.cashcare.service.dto.UserDTO;
import com.cashcare.service.mapper.UserMapper;
import com.jayway.jsonpath.JsonPath;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests for the {@link UserResource} REST controller.
 */
@AutoConfigureMockMvc
@WithMockUser(authorities = AuthoritiesConstants.ADMIN)
@SpringBootTest(classes = CashcareApp.class)
public class UserResourceTest {

    private static final String DEFAULT_LOGIN = "johndoe";

    private static final Long DEFAULT_ID = 1L;

    private static final String DEFAULT_PASSWORD = "$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC";

    private static final String DEFAULT_EMAIL = "johndoe@localhost";
    private static final String UPDATED_EMAIL = "jhipster@localhost";

    private static final String DEFAULT_FIRSTNAME = "john";
    private static final String UPDATED_FIRSTNAME = "jhipsterFirstName";

    private static final String DEFAULT_LASTNAME = "doe";
    private static final String UPDATED_LASTNAME = "jhipsterLastName";
    private static final LocalDate DEFAULT_BIRTHDATE = LocalDate.now();

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUserMockMvc;

    private User user;

    /**
     * Create a User.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which has a required relationship to the User entity.
     */
    public static User createEntity(EntityManager em) {
        User user = new User();
        user.setLogin(DEFAULT_LOGIN + RandomStringUtils.randomAlphabetic(5));
        user.setPassword(RandomStringUtils.random(60));
        user.setArchived(false);
        user.setEmail(RandomStringUtils.randomAlphabetic(5) + DEFAULT_EMAIL);
        user.setFirstName(DEFAULT_FIRSTNAME);
        user.setLastName(DEFAULT_LASTNAME);
        user.setBirthday(DEFAULT_BIRTHDATE);
        return user;
    }

    @BeforeEach
    public void initTest() {
        user = createEntity(em);
        user.setLogin(DEFAULT_LOGIN);
        user.setEmail(DEFAULT_EMAIL);
    }

    @Test
    @Transactional
    public void createAdmin() throws Exception {
        int databaseSizeBeforeCreate = userRepository.findAll().size();

        HashMap<String, String> mockMap = new HashMap<>();
        mockMap.put("password", DEFAULT_PASSWORD);
        mockMap.put("authorities", "ROLE_ADMIN");
        mockMap.put("firstName", DEFAULT_FIRSTNAME);
        mockMap.put("lastName", DEFAULT_LASTNAME);
        mockMap.put("birthday", DEFAULT_BIRTHDATE.toString());

        restUserMockMvc.perform(post("/api/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(mockMap)))
            .andExpect(status().isBadRequest());

        mockMap.put("email", DEFAULT_EMAIL);

        restUserMockMvc.perform(post("/api/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(mockMap)))
            .andExpect(status().isOk());

        // Validate the User in the database
        assertPersistedUsers(users -> {
            assertThat(users).hasSize(databaseSizeBeforeCreate + 1);
            User testUser = users.get(users.size() - 1);
            assertThat(testUser.getFirstName()).isEqualTo(DEFAULT_FIRSTNAME);
            assertThat(testUser.getLastName()).isEqualTo(DEFAULT_LASTNAME);
            assertThat(testUser.getEmail()).isEqualTo(DEFAULT_EMAIL);
        });

        restUserMockMvc.perform(post("/api/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(mockMap)))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    public void createUser() throws Exception {
        int databaseSizeBeforeCreate = userRepository.findAll().size();

        HashMap<String, String> mockMap = new HashMap<>();
        mockMap.put("password", DEFAULT_PASSWORD);
        mockMap.put("authorities", "ROLE_USER");
        mockMap.put("firstName", DEFAULT_FIRSTNAME);
        mockMap.put("lastName", DEFAULT_LASTNAME);
        mockMap.put("birthday", DEFAULT_BIRTHDATE.toString());
        mockMap.put("email", DEFAULT_EMAIL);
        mockMap.put("city", "Heidelberg");
        mockMap.put("country", "Deutschland");
        mockMap.put("house", "12a");
        mockMap.put("phone", "12345678");
        mockMap.put("postalCode", "12345");

        restUserMockMvc.perform(post("/api/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(mockMap)))
            .andExpect(status().isBadRequest());

        mockMap.put("street", "Karsstr.");

        restUserMockMvc.perform(post("/api/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(mockMap)))
            .andExpect(status().isOk());

        // Validate the User in the database
        assertPersistedUsers(users -> {
            assertThat(users).hasSize(databaseSizeBeforeCreate + 1);
            User testUser = users.get(users.size() - 1);
            assertThat(testUser.getFirstName()).isEqualTo(DEFAULT_FIRSTNAME);
            assertThat(testUser.getLastName()).isEqualTo(DEFAULT_LASTNAME);
            assertThat(testUser.getEmail()).isEqualTo(DEFAULT_EMAIL);
        });

        restUserMockMvc.perform(post("/api/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(mockMap)))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    public void updateUser() throws Exception {
        HashMap<String, String> mockMap = new HashMap<>();
        mockMap.put("password", DEFAULT_PASSWORD);
        mockMap.put("authorities", "ROLE_USER");
        mockMap.put("firstName", DEFAULT_FIRSTNAME);
        mockMap.put("lastName", DEFAULT_LASTNAME);
        mockMap.put("birthday", DEFAULT_BIRTHDATE.toString());
        mockMap.put("email", DEFAULT_EMAIL);
        mockMap.put("city", "Heidelberg");
        mockMap.put("country", "Deutschland");
        mockMap.put("house", "12a");
        mockMap.put("phone", "12345678");
        mockMap.put("postalCode", "12345");
        mockMap.put("street", "Karsstr.");

        MvcResult mr = restUserMockMvc.perform(post("/api/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(mockMap)))
            .andExpect(status().isOk()).andReturn();

        int databaseSizeBeforeCreate = userRepository.findAll().size();

        HashMap<String, String> mockMapUpdate = new HashMap<>();
        mockMapUpdate.put("firstName", UPDATED_FIRSTNAME);
        mockMapUpdate.put("lastName", UPDATED_LASTNAME);
        mockMapUpdate.put("email", UPDATED_EMAIL);
        mockMapUpdate.put("city", "Moskau");
        mockMapUpdate.put("country", "Russland");
        mockMapUpdate.put("house", "12ab");
        mockMapUpdate.put("phone", "123456789");
        mockMapUpdate.put("postalCode", "123456");

        String login = mr.getResponse().getContentAsString();
        // An entity with an existing ID cannot be created, so this API call must fail
        restUserMockMvc.perform(put("/api/users/" + login)
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(mockMapUpdate)))
            .andExpect(status().isOk());

        // Validate the User in the database
        assertPersistedUsers(users -> assertThat(users).hasSize(databaseSizeBeforeCreate));

        assertPersistedUsers(users -> {
            User testUser = users.get(users.size() - 1);
            assertThat(testUser.getFirstName()).isEqualTo(UPDATED_FIRSTNAME);
            assertThat(testUser.getLastName()).isEqualTo(UPDATED_LASTNAME);
            assertThat(testUser.getEmail()).isEqualTo(UPDATED_EMAIL);
        });
    }

    @Test
    @Transactional
    public void getAllUsers() throws Exception {
        // Initialize the database
        userRepository.saveAndFlush(user);

        // Get all the users
        MvcResult mr = restUserMockMvc.perform(get("/api/users?role=ROLE_USER")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk()).andReturn();

        assertThat(mr.getResponse().getContentAsString().contains("\"login\":\"user\""));

        mr = restUserMockMvc.perform(get("/api/users?role=ROLE_ADMIN")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk()).andReturn();

        assertThat(mr.getResponse().getContentAsString().contains("\"login\":\"admin\""));

        restUserMockMvc.perform(get("/api/users?role=ROLE")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest()).andReturn();
    }

    @Test
    @Transactional
    public void getCurrentUser() throws Exception {
        // Initialize the database
        //userRepository.saveAndFlush(user);

        HashMap<String, String> mockMap = new HashMap<>();
        mockMap.put("password", DEFAULT_PASSWORD);
        mockMap.put("authorities", "ROLE_USER");
        mockMap.put("firstName", DEFAULT_FIRSTNAME);
        mockMap.put("lastName", DEFAULT_LASTNAME);
        mockMap.put("birthday", DEFAULT_BIRTHDATE.toString());
        mockMap.put("email", DEFAULT_EMAIL);
        mockMap.put("city", "Heidelberg");
        mockMap.put("country", "Deutschland");
        mockMap.put("house", "12a");
        mockMap.put("phone", "12345678");
        mockMap.put("postalCode", "12345");
        mockMap.put("street", "Karsstr.");

        MvcResult mr = restUserMockMvc.perform(post("/api/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(mockMap)))
            .andExpect(status().isOk()).andReturn();
        String login = mr.getResponse().getContentAsString();

        String content = "{\"username\":\"user\",\"password\":\"user\"}";

        restUserMockMvc.perform(post("/api/authenticate")
            .contentType(MediaType.APPLICATION_JSON)
            .content(content)).andExpect(status().isUnauthorized()).andReturn();

        content = "{\"username\":\"user\",\"password\":\"admin\"}";

        mr = restUserMockMvc.perform(post("/api/authenticate")
            .contentType(MediaType.APPLICATION_JSON)
            .content(content)).andExpect(status().isOk()).andReturn();

        String jwt = "Bearer " + JsonPath.read(mr.getResponse().getContentAsString(), "id_token");
        // Get the user

        restUserMockMvc.perform(get("/api/account").header("Authorization", jwt))
            .andExpect(status().isBadRequest());

        content = "{\"username\":\"" + login + "\",\"password\":\"admin\"}";

        mr = restUserMockMvc.perform(post("/api/authenticate")
            .contentType(MediaType.APPLICATION_JSON)
            .content(content)).andExpect(status().isOk()).andReturn();

        jwt = "Bearer " + JsonPath.read(mr.getResponse().getContentAsString(), "id_token");
        // Get the user

        System.out.println(jwt);
        restUserMockMvc.perform(get("/api/account").header("Authorization", jwt))
            .andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void getUser() throws Exception {
        restUserMockMvc.perform(get("/api/account/admin"))
            .andExpect(status().isOk());

        restUserMockMvc.perform(get("/api/account/bad"))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void testUserDTOtoUser() {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(DEFAULT_ID);
        userDTO.setLogin(DEFAULT_LOGIN);
        userDTO.setFirstName(DEFAULT_FIRSTNAME);
        userDTO.setLastName(DEFAULT_LASTNAME);
        userDTO.setEmail(DEFAULT_EMAIL);
        userDTO.setArchived(false);
        userDTO.setAuthorities(Collections.singleton(AuthoritiesConstants.USER));

        User user = userMapper.userDTOToUser(userDTO);
        assertThat(user.getId()).isEqualTo(DEFAULT_ID);
        assertThat(user.getLogin()).isEqualTo(DEFAULT_LOGIN);
        assertThat(user.getFirstName()).isEqualTo(DEFAULT_FIRSTNAME);
        assertThat(user.getLastName()).isEqualTo(DEFAULT_LASTNAME);
        assertThat(user.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(user.getArchived()).isEqualTo(false);
        assertThat(user.getAuthorities()).extracting("name").containsExactly(AuthoritiesConstants.USER);
    }

    @Test
    public void testUserToUserDTO() {
        user.setId(DEFAULT_ID);
        Set<Authority> authorities = new HashSet<>();
        Authority authority = new Authority();
        authority.setName(AuthoritiesConstants.USER);
        authorities.add(authority);
        user.setAuthorities(authorities);

        UserDTO userDTO = userMapper.userToUserDTO(user);

        assertThat(userDTO.getId()).isEqualTo(DEFAULT_ID);
        assertThat(userDTO.getLogin()).isEqualTo(DEFAULT_LOGIN);
        assertThat(userDTO.getFirstName()).isEqualTo(DEFAULT_FIRSTNAME);
        assertThat(userDTO.getLastName()).isEqualTo(DEFAULT_LASTNAME);
        assertThat(userDTO.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(userDTO.getArchived()).isEqualTo(false);
        assertThat(userDTO.getAuthorities()).containsExactly(AuthoritiesConstants.USER);
        assertThat(userDTO.toString()).isNotNull();
    }

    @Test
    public void testAuthorityEquals() {
        Authority authorityA = new Authority();
        assertThat(authorityA).isEqualTo(authorityA);
        assertThat(authorityA).isNotEqualTo(null);
        assertThat(authorityA).isNotEqualTo(new Object());
        assertThat(authorityA.hashCode()).isEqualTo(0);
        assertThat(authorityA.toString()).isNotNull();

        Authority authorityB = new Authority();
        assertThat(authorityA).isEqualTo(authorityB);

        authorityB.setName(AuthoritiesConstants.ADMIN);
        assertThat(authorityA).isNotEqualTo(authorityB);

        authorityA.setName(AuthoritiesConstants.USER);
        assertThat(authorityA).isNotEqualTo(authorityB);

        authorityB.setName(AuthoritiesConstants.USER);
        assertThat(authorityA).isEqualTo(authorityB);
        assertThat(authorityA.hashCode()).isEqualTo(authorityB.hashCode());
    }

    private void assertPersistedUsers(Consumer<List<User>> userAssertion) {
        userAssertion.accept(userRepository.findAll());
    }

}
